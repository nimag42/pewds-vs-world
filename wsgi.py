#!/usr/bin/env python
import re
from string import Template
from wsgiref.simple_server import make_server

import requests

def app(environ, start_response):
    status = '200 OK'
    response_headers = [('Content-type','text/html')]
    start_response(status, response_headers)

    pewdsSub = 65056500
    try:
        sb = requests.get("https://socialblade.com/youtube/user/pewdiepie")
        cnt = re.search('youtube-stats-header-subs".+>([0-9]+)</span>', sb.text)
        if cnt.group(1):
            pewdsSub = cnt.group(1)
    except:
        pass

    with open('page.html') as f:
        page = Template(f.read())
        return page.safe_substitute(pewds_subscribers_count=65056500)



if __name__ == '__main__':
    srv = make_server('localhost', 8080, app)
    srv.serve_forever()
